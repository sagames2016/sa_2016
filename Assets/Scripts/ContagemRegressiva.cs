﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ContagemRegressiva : MonoBehaviour {

	public int numero_Contagem;
	public int numero_Impressao;

	public float numero_Atual_Contagem;
	public float tempo_Contagem;

	public float valorDelta;
    

	public Text texto;
   
    
   
	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
		Pause.pausa = false;
		GameManager.pausado = false;
		resetarVariaveis (3, 0, 0);
       
	}
	
	// Update is called once per frame
	void Update () {
       
            numero_Impressao = (int)numero_Atual_Contagem;
            if (numero_Impressao >= 2)
            {
			texto.text = "" + numero_Impressao;
            }
            else {// if(numero_Impressao<2){
                texto.text = "" + numero_Impressao ;
            }

            if (numero_Atual_Contagem > numero_Contagem)
            {
                numero_Atual_Contagem -= Time.deltaTime;
            }
            if (numero_Impressao <= numero_Contagem)
            {
                resetarVariaveis(3, 0, 0);
                if (numero_Impressao == 0 && numero_Contagem == 0)
                {
					
					SceneManager.LoadScene("FaseComPisoTempleRun");
					Pause.pausa = true;
                    Destroy(this);
                }
            }
        
	}

	public void  resetarVariaveis(int a, int b, int c) 
	{
		numero_Atual_Contagem = a;
		numero_Contagem = b;
		numero_Impressao = c;

	}

   
}
