﻿using UnityEngine;
using System.Collections;

public class Moeda : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!GameManager.pausado)
			transform.Rotate(new Vector3(0, 5, 0));
		   
	}

	public void OnTriggerEnter(Collider outro){
		
		if (outro.gameObject.tag == "Player") {
			//GameManager.moedaScore++;
            GameObject.Find("GameManager").GetComponent<GameManager>().moedaScore++;
            Destroy (gameObject);
		}
	}
}
