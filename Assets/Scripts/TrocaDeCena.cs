﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TrocaDeCena : MonoBehaviour {


    public GameObject telaPrincipal;//tela principal
    public GameObject telaRecord;// tea do Record;
    public GameObject telaComfirmação; //tela de confirmação para deletar record
    private int teste;

    public Text recordText;




	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        recordText.text = "" + PlayerPrefs.GetInt("HighScore");
	
	}

	public void TrocaCena(string nome){
		
		SceneManager.LoadScene (nome);
	}

    public void resetRecord(string decisao)    {
        if(decisao == "yes"){
            PlayerPrefs.SetInt("HighScore",0);
            telaComfirmação.SetActive(false);
            PlayerPrefs.SetInt("1", 0);
            PlayerPrefs.SetInt("2", 0);
        }else{
            telaComfirmação.SetActive(false);
        }
        

    }

    public void navegaCanvas(string estado){
        switch (estado){
            case "principal":
                telaPrincipal.SetActive(true);
                telaRecord.SetActive(false);
                break;
            case "record":
                telaPrincipal.SetActive(false);
                telaRecord.SetActive(true);
                break;
            case "confirmacao":
                telaComfirmação.SetActive(true);
                break;
            default:
                break;
        }
    }

	public void ExitGame(){
		Application.Quit();
	}
}
