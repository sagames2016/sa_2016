﻿using UnityEngine;
using System.Collections;

public class RotacaoEixo : MonoBehaviour {

	public float velocidadeX;
	public float velocidadey;
	public float velocidadeZ;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if(!GameManager.pausado)
			transform.Rotate(new Vector3(velocidadeX, velocidadey, velocidadeZ));
	}
}
