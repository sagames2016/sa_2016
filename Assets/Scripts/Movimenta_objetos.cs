using UnityEngine;
using System.Collections;


/*
*	Classe responsavel por criar e movimentar 
*	objetos que fazem colisao com o personagem
*
*
**/


public class Movimenta_objetos : MonoBehaviour {

	Vector3 newPosition;
	public GameManager gameManager;
	public int tamanhoArray=13;
	public int sorteioAnteriorBuracoLateral;
	public int contaSorteioAnterior;
	public GameObject[] objetosChao = new GameObject[11];
	/** 
	 * 
	 * public GameObject pedraMeioEsquerda;
	 * public GameObject pedraEsquerda;
	 * public GameObject pedraMeioDireita;
	 * public GameObject pedraDireita;
	 * public GameObject espinhoCentral;
	 * public GameObject espinhoMeioEsquerda;
	 * public GameObject espinhoEsquerda;
	 * public GameObject espinhoMeioDireita;
	 * public GameObject espinhoDireita;
	 * public GameObject chaoVazio;
	 */
	public int objetosCriados=0;

	public int sorteio;
	public int sorteioAnterior=GameManager.sorteioAnterior;

	public float posicaoXobjeto= 0;
	public float velocidade;// = GameManager.velocidade;
	public int contador = 0;
	public float distancia_Nascimento;

	// Use this for initialization
	void Start () {

		transform.Rotate(0,0,0);
	}

	// Update is called once per frame
	void Update () {

		if (!GameManager.pausado) {
			
			Vector3 newPosition = transform.position;
			newPosition.z += (Time.deltaTime + 1 * -1); // velocidade dos objetos 

			transform.position = newPosition;
		} else if (GameManager.pausado) {
		}


	}

	public void OnTriggerEnter(Collider outro){
		sorteio = Random.Range (0, tamanhoArray);

		if (outro.gameObject.tag == "cria_objeto"&&objetosCriados==0) {
			objetosCriados = 1;
			sorteio = Random.Range (0, tamanhoArray);
			//if (GameManager.sorteioAnterior==0&&sorteio==0) {
			//		sorteio = Random.Range(1,6);
					
			//}
			//if(contador>=1){
				
			if (sorteio == 0) {
				Instantiate (objetosChao [sorteio], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				GameManager.sorteioAnterior = sorteio;
				sorteioAnterior = sorteio;
				contaSorteioAnterior = 0;
				GameManager.contador_de_vazios++;
			}
			if (sorteio != 0 && sorteio != 11 && sorteio != 12) {
				Instantiate (objetosChao [sorteio], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				//chao_obstaculo
				sorteioAnterior = sorteio;
				contaSorteioAnterior = 0;
				GameManager.sorteioAnterior = sorteio;
			}
			if (contaSorteioAnterior <= 30 && sorteioAnterior == 11) {
				Instantiate (objetosChao [11], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				contaSorteioAnterior++;
			}
			if (contaSorteioAnterior <= 30 && sorteioAnterior == 12) {
				Instantiate (objetosChao [12], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				contaSorteioAnterior++;
					
			}
			if (sorteio == 11 || sorteio == 12) {
				Instantiate (objetosChao [sorteio], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				sorteioAnteriorBuracoLateral = sorteio;

			}
			if (contaSorteioAnterior >= 12) {
				Instantiate (objetosChao [0], new Vector3 (posicaoXobjeto, transform.position.y, distancia_Nascimento), Quaternion.identity);
				contaSorteioAnterior = 0;
				sorteioAnteriorBuracoLateral = 0;
			}
			if (sorteio == 13) {
				
			}

			contador++;	
				
			//}else if (contador>=2){
			//sorteioAnterior=GameManager.sorteioAnterior;
			//}

			
						

		}
		if (GameManager.contador_de_vazios == 3) {
				//	Instantiate (cameraTroca, new Vector3 (transform.position.x, transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);
			GameManager.contador_de_vazios = 0;
		}

		if (outro.gameObject.tag == "destroi_objeto"){
			distancia_Nascimento = 98273;
			Destroy(gameObject);

			if (outro.gameObject.tag == "Player") {
				
				Destroy(gameObject);
			}
		}
	}
}
