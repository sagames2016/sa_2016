﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SelectChar : MonoBehaviour {

    public GameObject[] Characters;
    public int[] prices;
    public GameObject buttonBlock;
    
    public GameObject personagem;
    public int index;

    public Text Moedas;
    public Text price;


    AudioSource somBlock;

    bool startTimer;
    int timerDelay;



    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        startTimer = false;
        timerDelay = 30;
        somBlock = GameObject.Find("Button_Avançar").GetComponent<AudioSource>();

        index = 0;
        PlayerPrefs.GetInt("Moedas");
        PlayerPrefs.SetInt("Character", index);
        PlayerPrefs.Save();
        

    }

   
	
	// Update is called once per frame
	void Update () {
        blockCharacter();
        Moedas.text = "" + PlayerPrefs.GetInt("Moedas") ;
       
        if (!personagem){
           personagem = (GameObject)Instantiate(Characters[index], new Vector3(0, 1.5f, 0), Quaternion.identity);

        }


        if(startTimer == true){
            timerDelay--;
            print("" + timerDelay);
            if (timerDelay<=0){
                GameObject.Find("Button_Avançar").GetComponent<Animator>().SetBool("isReady", false);
                timerDelay = 30;
                startTimer = false;
            }
        }

       
      
         

    }

    public void TradeCharacter(string direction){
        if(direction == "Direita"){
            index += 1;
            if(index >= Characters.Length){
                index = 0;
            }
             Destroy(personagem);
        }else{
            index -= 1;
            if (index<0){
                index = Characters.Length - 1;
               
            }
            Destroy(personagem);
        }
    }


    public void trocaDeCena(string direcao) {
        if (direcao == "Menu")
        {
			Destroy (gameObject);
			SceneManager.LoadScene("Main_Menu");
        }
        if (direcao == "avancar" && PlayerPrefs.GetInt("" + index) == 1)
        {
			Destroy (gameObject);
            PlayerPrefs.SetInt("Character", index);
            PlayerPrefs.Save();
			SceneManager.LoadScene("Contagem_Regressiva");
        }else{
            startTimer = true;
            GameObject.Find("Button_Avançar").GetComponent<Animator>().SetBool("isReady", true);
            somBlock.Play();
            
        }
     
    }

   

    void blockCharacter(){
        if (PlayerPrefs.GetInt("" + index) == 1){
            buttonBlock.SetActive(false);
        }else if (PlayerPrefs.GetInt("" + index) == 0){
            buttonBlock.SetActive(true);
            price.text = "" + prices[index];
        }
    }

    public void unblock(){
        if (PlayerPrefs.GetInt("Moedas") >= prices[index] && PlayerPrefs.GetInt(""+index)==0){
            PlayerPrefs.SetInt("" + index, 1);
            buttonBlock.SetActive(false);
            PlayerPrefs.SetInt("Moedas", PlayerPrefs.GetInt("Moedas") - prices[index]);
        }

    }



	
}
