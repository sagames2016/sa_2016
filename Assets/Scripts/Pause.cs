﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/*
 * Classe Pause criada para colocar a logica do botao pause do jogo
 * se nao tivesse o botao pause esta logica poderia estar no GameManager,
 * esta seria uma atualizacao a ser estudada caso o jogo necessite de mais potencia
 * 
 * */

public class Pause : MonoBehaviour {

    /*
     *MAICON********
       esses objetos de tela e button tem que ser Gameobject, para nao perder a referencia quando
       forem desativadas.
       nao esquece os importes 
     
    */

    public GameObject telaDePause;
    public GameObject buttonPause;
    

    public static bool pausa;



	// Use this for initialization
	void Start () {

        GameManager.pausado = pausa;
	}
	
	// Update is called once per frame
	void Update () {
        
       // pausa = !GameManager.pausado;//deixa o valor de pausado diferente do valor atual
        
        if (pausa) {
            Time.timeScale = 0;

        } else if (!pausa) {
            Time.timeScale = 1;
			pausa=false;
		
        }
       
        GameManager.pausado = pausa;
    }

    public void setPause() {
        pausa = !GameManager.pausado;//deixa o valor de pausado diferente do valor atual

        if (pausa)
        {
        
            Time.timeScale = 0;
            buttonPause.SetActive(false);// adicionei isso aqui tbm só pra nao esquecer uashdush 
            telaDePause.SetActive(true);//>>>>>>>>>>>>>>>>>>>>>>
        }
        else if (!pausa)
        {
	;
			GameManager.jogador_esta_vivo = true;
			            
            buttonPause.SetActive(true);//>>>>>>>>>>>>>>>>
            telaDePause.SetActive(false);//>>>>>>>>>>>>>>>>
			Time.timeScale = 1;
        }

        GameManager.pausado = pausa;
    }


    /*
        usei a classe do pause pra colocar as funções dos buttons .. 
    */
    public void restartLevel()
    {
		SceneManager.LoadScene("Contagem_Regressiva");

    }

    public void menu()
    {
        SceneManager.LoadScene("Main_Menu");

    }


}
