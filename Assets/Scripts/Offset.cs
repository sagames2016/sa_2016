﻿using UnityEngine;
using System.Collections;

public class Offset : MonoBehaviour {

    public SkinnedMeshRenderer ren;
	// Use this for initialization
	void Start () {
        ren = GetComponent<SkinnedMeshRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

        float offset = Time.time * -1f;
        ren.material.mainTextureOffset = new Vector3(offset,0,offset);
	}
}
