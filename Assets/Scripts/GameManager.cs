﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


/*
 *Classe GameManager - Esta e a classe para controlar o Jogo de modo geral, 
 *  	status de cenas de jogador etc.
 *
 *
 **/
public class GameManager : MonoBehaviour {




    //variaveis de status do jogo	


    //public TextMesh highscores;

    //*Maicon 
    //variaveis de texto 
    public Text scoreText;
    public Text scoreGText;
    public Text recordText;
	public Text moedaText;

	public static bool jogador_esta_vivo;//verifica se jogador esta vivo para liberar a logica dos controles na classe CharacterMovement
	public bool primeiro_vez_que_joga;//variavel sera usada para controlar se vai mostrar um tutorial de coomo o jogador joga 
	public bool esta_online;//para habilitar assets online(share, banner e advideos)
    public static bool correndo = true;//para controlar status atual do jogador - talvez devesse estar na classe CharacterMovement
	public static bool pulando = false;//para controlar status atual do jogador - talvez devesse estar na classe CharacterMovement
	public static bool pausado;//para mostrar status atual do jogo

    public static int escolheViverAtivo=0;//a principio nao esta sendo usada
	public int moedaScore;

   

	//variaveis abaixo ainda nao analisada, mas podem estar sendo usadas, nao apagar
    public static int recorde;
	public int contador_1;//
	public int bonus_atual = 1;
	public int tempo_por_metro	 = 10;
	public  int contador_de_pontos;
	public static int sorteio_anterior=0;
	public static int sorteioAnterior;
	public float velocidade;
	public float velocidade_Cenario;
	public float velocidade_Objetos;

	public static int contador_de_vazios;

	public float timeRemaining = 5;

    public   GameObject telaGameOver;// tive que retirar referencia da tela de game over do personagem pq estava perdendo referencia ,no prefab.

    public GameObject[] chares;//array dos personagens.

	// Use this for initialization
	void Start () {
       
		jogador_esta_vivo = true;
        pausado = false;
        telaGameOver.SetActive(false);
        Instance_Char();
	}

    // Update is called once per frame
    void Update() {
        //**********Maicon**********************
        scoreText.text = "Score: " + contador_de_pontos;
        scoreGText.text = "" + contador_de_pontos;
        recordText.text = "" + PlayerPrefs.GetInt("HighScore");
		moedaText.text = "" + moedaScore;
        if (jogador_esta_vivo && pausado == false)
        {
            //Destroy(gameObject);


            contador_de_pontos += (int)((Time.deltaTime / tempo_por_metro) + bonus_atual);

            //GetComponent<TextMesh>().text = "Ponto:     " + contador_de_pontos;
        }

        if (contador_de_pontos > recorde) {
            recorde = contador_de_pontos;
        }


        if (contador_de_pontos > PlayerPrefs.GetInt("HighScore")) {
            PlayerPrefs.SetInt("HighScore", contador_de_pontos);
        }
        //highscores.text ="Recorde:   " + PlayerPrefs.GetInt("HighScore");

        if (Input.GetKeyDown(KeyCode.F1))
        {
            // PlayerPrefs.SetInt("HighScore", 0);
        }
    }

        public void Instance_Char(){
            int index = PlayerPrefs.GetInt("Character");
            Instantiate(chares[index], new Vector3(0, 0, 0), Quaternion.identity);// peg o index da cena SelecChar e instancia aqui.
        }

	
    public void SomaMoeda(){
        int totalmoedas = PlayerPrefs.GetInt("Moedas") + moedaScore;
        PlayerPrefs.SetInt("Moedas", totalmoedas);
        PlayerPrefs.Save();
    }


}
